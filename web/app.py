from flask import Flask, render_template, abort
import config
import logging
import os

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration

def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    if options.DOCROOT != 'pages':
        log.warning(("Docroot {} selected. "
                     " Document Root should be set to pages for the grading to work correctly.").format(options.DOCROOT))

    return options

app = Flask(__name__, template_folder="errors")
options = get_options()

@app.route("/<endpoint>")
def dynamic(endpoint):

    if ".." in endpoint:
        abort(403)
    elif "//" in endpoint:
        abort(403)
    elif "~" in endpoint:
        abort(403)

    path = options.DOCROOT + '/' + endpoint
    if os.path.exists(path) and (not os.path.isdir(endpoint)):
        with open(path, 'r') as myfile:
            return myfile.read()
    else:
        abort(404)

@app.errorhandler(404)
def page_not_found(error):
    return render_template("404.html"), 404

@app.errorhandler(403)
def page_prohibited(error):
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0', port=options.PORT)
